import networkx as nx
import numpy as numpy
import matplotlib.pyplot as plt
from collections import deque

def BFS(G, source):

    node_List = list(G.nodes())
    label_List = [0 for node in node_List]
    distance = [-1000 for node in node_List]

    if source not in G.nodes():
        raise Exception ("Source node must be in the graph")

    label_List[source] = 1
    distance[source] = 0
    unexplored = deque([source])


    while unexplored:

        current_node = unexplored.popleft()
        neighbor_nodes = G.neighbors(current_node)
        
        for node in neighbor_nodes:
            if label_List[node] == 0:
                unexplored.append(node)
                label_List[node] = 1
                distance[node] = distance[current_node] + 1
    
    return label_List, distance
    
def DFS(G, source):

    node_List = list(G.nodes())
    label_List = [0 for node in node_List]
    distance = [-1000 for node in node_List]

    if source not in G.nodes():
        raise Exception ("Source node must be in the graph")

    label_List[source] = 1
    distance[source] = 0
    unexplored = deque([source])

    while unexplored:

        current_node = unexplored.pop()
        neighbor_nodes = G.neighbors(current_node)
        
        for node in neighbor_nodes:
            if label_List[node] == 0:
                unexplored.append(node)
                label_List[node] = 1
                distance[node] = distance[current_node] + 1
    
    return label_List, distance

def DFS_recursive_help(G, vertex, label_list, previous_distance, distance):

    label_list[vertex] = 1
    neighbor_nodes = G.neighbors(vertex)
    
    distance[vertex] = previous_distance + 1

    for node in neighbor_nodes:
        if label_list[node] == 0:
            DFS_recursive_help(G, node, label_list, distance[vertex], distance)
    
    return label_list, distance

def DFS_recursive(G, source):

    node_List = list(G.nodes())
    label_List = [0 for node in node_List]
    distance = [-1000 for node in node_List]

    return DFS_recursive_help(G, source, label_List, -1, distance)



#Every time, choose the colest neighbor node to visit
#Only work for a connected graph 
def dijkstra(G, s):

    #Initialize dictionaries
    dinit = 10**6
    Edict = {} #Explored nodes
    Udict = {} #Uneplroed nodes

    for n in G.nodes():
        Udict[n] = dinit
    Udict[s]=0


    while len(Udict) > 0:
        dmin = dinit
        for n, w in Udict.items():
            if w < dmin:
                dmin = w
                nmin = n
        print (Udict.items())
        Edict[nmin] = Udict.pop(nmin)

        for n,w in G.adj[nmin].items():
            if n in Udict:
                dcomp = dmin + w['weight']
                if dcomp < Udict[n]:
                    Udict[n] = dcomp
    return Edict

